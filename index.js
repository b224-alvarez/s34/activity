const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

let users = [
  {
    username: "johndoe",
    password: "johndoe1234",
  },
  {
    username: "janesmith",
    password: "janesmith1234",
  },
];

//1.
app.get("/home", (request, response) => {
  response.send("Welcome to the home page");
});

//2.
app.get("/users", (request, response) => {
  response.send(users);
});

//3.
app.delete("/delete-user", (request, response) => {
  let message = `User ${request.body.username} has been deleted.`;

  console.log("123");
  for (let i = 0; i < users.length; i++) {
    if (
      request.body.username == users[i].username &&
      request.body.password == users[i].password
    ) {
      delete users[i];

      break;
    } else {
      message = "User does not exist";
    }
  }
  response.send(message);
});

app.listen(port, () => console.log(`Server running at port ${port}`));
